public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem("abc", "pen", 5, 1.2);
        InvoiceItem invoiceItem2 = new InvoiceItem("def", "paper", 8, 1.5);
        System.out.println(invoiceItem1);
        System.out.println(invoiceItem2);
        System.out.println("Tong gia cua hoa don 1: " + invoiceItem1.getTotal());
        System.out.println("Tong gia cua hoa don 2: " + invoiceItem2.getTotal());
    }
}
