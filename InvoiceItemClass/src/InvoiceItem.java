public class InvoiceItem {
    private String id;
    private String desc;
    private int qty;
    private double unitPrice;
    // khởi tạo đủ tham số
    public InvoiceItem(String id,
    String desc, int qty, double unitPrice) {
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }
    // getter setter
    public String getId() {
        return id;
    }
    public String getDesc() {
        return desc;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    // method tính tổng tiền
    public double getTotal() {
        return unitPrice * qty;
    }
    @Override
    public String toString() {
        return "InvoiceItem [desc=" + desc
            + ", id=" + id
            + ", qty=" + qty
            + ", unitPrice=" + unitPrice
            + "]";
    }
}
